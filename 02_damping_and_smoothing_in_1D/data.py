import numpy as np

def get_data():
	# Continuous bedrock model with resolution of 1 m (only used for plotting)
	x = np.arange(101)
	z = 0 - 6 + np.sin(x * 2 * np.pi / 50) + 0.04 * x

	x_b = np.linspace(0, 100, 10) # Measurement positions
	z_b = 0 - 6 + np.sin(x_b * 2 * np.pi / 50) + 0.04 * x_b # Discrete model parameters

	# Generate measurements
	v = 2500  # P-wave velocity [m/s]
	d_orig = 2 * np.abs(z_b) / v

	# Add noise
	np.random.seed(1234)
	d = d_orig + np.random.normal(scale=0.05 * d_orig)

	return d

def get_true_topography():
	x = np.arange(101)
	z = 0 - 6 + np.sin(x * 2 * np.pi / 50) + 0.04 * x

	return x, z

def get_true_model():
	x_b = np.linspace(0, 100, 10) # Measurement positions
	z_b = 0 - 6 + np.sin(x_b * 2 * np.pi / 50) + 0.04 * x_b # Discrete model parameters

	return z_b