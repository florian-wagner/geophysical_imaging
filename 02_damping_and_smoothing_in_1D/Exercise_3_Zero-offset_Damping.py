#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
from numpy.linalg import inv
import matplotlib.pyplot as plt
plt.style.use("seaborn-notebook")


# # Exercise 3 (Nov. 11, 2021)
# ---

# # Zero-offset profiling
# 
# <img src="https://fwagner.info/lehre/updated_fig.png" style="width: 70%;">
# 
# A zero-offset seismic depth sounding is conducted with the aim of detecting the subsurface topography of a bedrock, overlain with sediments (see figure above). The surface is located at $z = 0$, and measurements are conducted in equidistant intervals (positions $x_b$).
# 
# A simplified seismic model view is assumed: A seismic signal is generated at a source location and the signal is then assumed to travel downwards and reflected at the top of the bedrock. A receiver at the source location then picks up the reflected signal. Measurements are thus travel times (in seconds of the seismic wave from source to bedrock and back $\rightarrow$ TWT, Two-way traveltimes). The P-wave velocity of the sediment layer is assumed to be $v = 2500$ m/s.
# 
# A transect of $100$ m length is measured, starting a $x=0$, ending at $x=100$ m. Measurements are equally spaced (with $N_d$ the number of measurements - changes depending on the subtask). The bedrock topography in reality is continuous ($z$); however, for the sake of simplicity (and applicability), assume a finite number of bedrock segments, with a constant depth for each segment ($z_b$) as model parameters.

# ### a) Describe the model parameters, data parameters, and auxiliary variables and their units. Under which circumstances is the problem unique, over-, mixed-, or under-determined?

# ### b) Uniquely-determined problem
# 
# Formulate the linear inverse problem for the *unique* (also known as even-determined) problem, given 10 equally-spaced measurements. Adjust the number of bedrock segments accordingly.
# 
# **Estimate the bedrock topography** for the following data & measurement positions:

# In[2]:


import data
d = data.get_data() # Importing the measured data
x_b = np.linspace(0, 100, 10) # Creating mesurement positions
d, x_b


# In addition to the data and measurements we know the seismic velocity $v = 2500$, e.g. from borehole measurements. From these parameters, we try to estimate $z_b$. Perform the inversion by setting up a G matrix and using the appropriate mathematical solution (hint: diagonal matrices can be created conveniently by first creating an [identity matrix](https://numpy.org/doc/stable/reference/generated/numpy.eye.html))
# 
# Plot the resulting (discrete) topography using the `plt.step` [function](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.step.html) (with the argument `where="mid"`). Also have a look at the [axhline](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.axhline.html) and [vlines](https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.vlines.html) functions to plot the earth surface and the raypaths. Compare it to the true topography, which you can obtain via `data.get_true_topography()`.

# ### c) Damping
# 
# Now assume an under-determined problem, with only 4 measurements and 10 bedrock segments (i.e., model parameters). Estimate the bedrock depth for the sparse data set using the **damped-least squares solution** (often used for mixed-determined problems). Remember to keep the number of parameters (of the bedrock depth) the same as in (b). You can use the code snippet below to only use a subset of the data from task b (and also just a subset of rows in $\mathbf{G}$):
# 
# ``` python
# x_b_sparse = x_b[::3] # every third measurement location
# d_sparse = d[::3] # every third measurement
# ```
# 
# Use damping constraints ($\mathbf{W}_m = I$) to solve the least-squares problem. Vary the regularization parameter between $10^{-10}$ and $1$ and describe the results. Which value would you choose? Plot the final bedrock estimation.

# What happens if the damping factor is zero?

# ---
# ### Bonus question:
# 
# Compare the result of the damped least-squares solution (with a small damping factor) to the minimum-norm solution.
