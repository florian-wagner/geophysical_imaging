#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
plt.style.use("seaborn-notebook")


# # Exercise 3: Traveltime and regularization in two dimensions (Dec. 2, 2021)
# ---
# 
# Assume straight ray paths between transmitter and receiver on a two-dimensional
# model space. The travel time $t$ of the $q$-th ray path can be expressed as:
# 
# $$ t_q = \int_{L_q} \frac{1}{v(l)} dl \approx \sum_i s_i l_i, $$
# 
# where $v$ is the velocity and $L_q$ the total travel path of this ray, $l_i$
# are the individual ray paths in each cell, and $s_i$ are the discrete and
# piecewise constant slowness values. To do a forward computation, the travel
# times through each discrete cell can be determined geometrically and formulated
# in matrix notation as:
# 
# $$ \mathbf{t} = \mathbf{G} \mathbf{s}. $$
# 
# Here, the cumulated travel times of all rays form the vector $\mathbf{t}$, the
# slowness values of all cells are located in $\mathbf{s}$ and the ray paths of every
# ray form the matrix $\mathbf{G}$.  The matrix $\mathbf{G}$ contains one row per ray and
# one column per model cell. Accordingly, measured travel times $\mathbf{t}$ are
# associated with the data vector $\mathbf{d}$, and slowness values $\mathbf{s}$ are
# associated with the model parameters $\mathbf{m}$. The corresponding geometry is displayed in the figure below.
# 
# <img src="http://www2.geo.uni-bonn.de/~wagner/2dsimple.png" style="width:40%">
# 
# *[Figure](http://www2.geo.uni-bonn.de/~wagner/2dsimple.png): Measurement setup of a simple travel time tomography measurement. Green lines indicate ray paths of the individual measurements (with the label dX being the $X^{th}$ measurement, located at the end of the path). Parameter numbers are given in the lower-right corner of each cell (zero indexed).* 

# ### a) Solve the inverse problem, i.e. estimate the slowness distribution based on the traveltimes. Do you need regularization to solve the inverse problem? Construct the first-order smoothing matrix.
# 
# Test if your smoothing matrix is correct by multiplying it with a homogeneous models ([`np.ones`](https://numpy.org/doc/stable/reference/generated/numpy.ones.html?highlight=ones#numpy.ones)) and for comparison with random models ([`np.random.randn`](https://numpy.org/doc/stable/reference/random/generated/numpy.random.randn.html?highlight=randn#numpy.random.randn)). What is the model norm ([`np.linalg.norm`](https://numpy.org/doc/stable/reference/generated/numpy.linalg.norm.html?highlight=linalg%20norm#numpy.linalg.norm)) $||\mathbf{W_m m_\text{homogeneous}}||_2$? What should it be?

# ### b) Construct the generalized inverse $\mathbf{G}^{-g}$ first 5 measurements only.
# 
# Estimate the model parameters for both setups given the data vector from the previous exercise. 
# 
# **Plotting hint**:
# 
# You can visualize the true and estimated model on the model grid.
# 
# The model vector is always used as a vector in the calculations (also in 2D and 3D). In this 2D example, we can make use of the reshape method. So assume we want to visualize the number of the model parameters, we can do this:
# 
# ``` python
# import numpy as np
# import matplotlib.pyplot as plt
# 
# model1d = np.arange(25) # Just the model indices
# model2d = model1d.reshape((5, 5))[::-1] # rearrange to 5,5 and flip upside down
# 
# # Visualize the model as a colored 2D matrix
# fig, ax = plt.subplots()
# im = ax.imshow(model2d, interpolation='none', extent=[0, 5, 0, 5])
# cbar = fig.colorbar(im, ax=ax)
# cbar.set_label("Model parameter numbering")
# ```

# ### c) Compare the model estimate using **damping and smoothing** to the true model in terms of absolute relative deviation. **Which model parameter is closest to the true model?** (You don't need to take a look at all parameters manually if you use the [argmin](https://numpy.org/doc/stable/reference/generated/numpy.argmin.html?highlight=argmin#numpy.argmin) function.)

# ### d) Calculate the coverage (or cumulative sensitivity), i.e. the sum of all ray path segments per model cell for the small and the full setup. Visualize the coverage on the model grid. Which cell has the highest coverage? Is it the same cell/model parameter, which is closest to the true model?

# #### For 5 measurements

# #### For all 9 measurements

# #### e) Calculate the model resolution for both setups. Visualize the diagonal of the resolution matrix on the model grid. Which cell has the highest resolution? Is it the same cell/model parameter, which is closest to the true model?
