#!/usr/bin/env python
# coding: utf-8

# # Exercise 2 - Learning NumPy - Solutions
# `numpy` is the basis for numerical scientific computing in Python. If you want to work with data in Python, understanding how NumPy works is key.
# 
# It contains among other things:
# 
# - a powerful N-dimensional array object
# - sophisticated (broadcasting) functions
# - tools for integrating C/C++ and Fortran code
# - useful linear algebra, Fourier transform, and random number capabilities
# 
# You can find the numpy reference [here](http://docs.scipy.org/doc/numpy/reference/index.html#reference).

# 1) Import the numpy package under the name ``np``

# 2) Create a null vector of size 10 (Hint: See
# [`np.zeros`](http://docs.scipy.org/doc/numpy/reference/generated/numpy.zeros.html))

# 3) Create a null vector of size 10 except the fifth value, which is supposed to be 1

# 4) Create a vector with values ranging from 10 to 99 (Hint: See [`np.arange`](http://docs.scipy.org/doc/numpy/reference/generated/numpy.arange.html))

# 5) Create a 3x3 matrix with values ranging from 0 to 8 (Hint: See [`np.reshape`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.ndarray.reshape.html#numpy.ndarray.reshape))

# 6) Find indices of non-zero elements from \[1,2,0,0,4,0\] (Hint: See
# [`np.nonzero`](http://docs.scipy.org/doc/numpy/reference/generated/numpy.nonzero.html))

# 7) Declare a 10x10 identity matrix (Hint: See [`np.eye`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.eye.html))

# 8) Declare a 10x10x10 array with random values (Hint: See [Random
# sampling](https://docs.scipy.org/doc/numpy/reference/random/index.html))

# 9) Create a 10x10 matrix with row values ranging from 0 to 9 (Hint: Make use of [Broadcasting](https://docs.scipy.org/doc/numpy/user/basics.broadcasting.html))

# 10) Create a random vector of size 100 and sort it (Hint: See [Sorting, searching and counting](https://docs.scipy.org/doc/numpy/reference/routines.sort.html))

# 11) Create a random vector with 1000 entries and calculate its mean, median, and standard deviation. (Hint: See [Statistics](https://docs.scipy.org/doc/numpy/reference/routines.statistics.html))

# 12) Create a random vector of size 100 and replace the maximum value by 05 (Hint: See [Sorting, searching and counting](https://docs.scipy.org/doc/numpy/reference/routines.sort.html))

# 13) Reverse a vector (Hint: `[::-1]`)

# 14) Create a 1-D array of 50 evenly spaced elements between 3. and 10., inclusive. (Hint: Use [`np.linspace`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.linspace.html))

# 15) Create a 1-D array of 50 element spaced evenly on a log scale between 3. and 10., exclusive. (Hint: Use [`np.logspace`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.logspace.html) or [`np.geomspace`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.geomspace.html))

# 16) Create two two-dimensional arrays. Multiply them element-wise (`*`) and calculate the dot product (`@`).

# ___________________________________________________________________________________________________________________
# 
# ## Bonus Questions

# 17) Calculate sine, cosine, and tangent of x, element-wise. (Hint: See [Mathematical functions](https://docs.scipy.org/doc/numpy/reference/routines.math.html))

# 18) Create a random array (20 x 20) and declare a value. Now find the closest value (to the given scalar) in that array. (Hint: Use [`np.abs`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.absolute.html) and [`np.argmin`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.argmin.html))

# 19) Convert x from radian to degrees. Search for the corresponding numpy function with [`np.lookfor`](https://docs.scipy.org/doc/numpy/reference/generated/numpy.lookfor.html).
