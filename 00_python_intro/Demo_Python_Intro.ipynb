{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# The Python Language\n",
    "## [Basic numeric types](https://docs.python.org/3/library/stdtypes.html#typesnumeric)\n",
    "\n",
    "If you are not sure about the type of an object, you can use the built-in function [`type()`](https://docs.python.org/3/library/functions.html#type). Let's create some variables and then check their type."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Integer\n",
    "\n",
    "Integer numbers (positive or negative whole numbers) are of type `int`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 4\n",
    "type(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Float\n",
    "Floating point numbers (with fractional parts) are of type `float`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = 2.1\n",
    "type(c) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Complex\n",
    "You can also work with complex numbers, composed of a real and an imaginary part. The corresponding type is `complex`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 1.5 + 0.5j\n",
    "\n",
    "print(\"Real part:\", a.real, \"Imaginary part:\", a.imag)\n",
    "\n",
    "type(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Boolean\n",
    "Booleans are a subtype of integers. A variable of type `bool` can either be set to `False` or `True`. This data type is used to test whether the result of an expression is true or false."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test = 3 > 4\n",
    "type(test)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"The expression above is \", test)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `not` operator can be used to negate a boolean value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "not test"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Arithmetic operators"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can use the Python interpreter as a simple [calculator](https://docs.python.org/3/tutorial/introduction.html?highlight=calculator#using-python-as-a-calculator). To test this, have a look at the basic arithmetic operators:\n",
    "\n",
    "| Symbol | Task Performed |\n",
    "|---|---|\n",
    "| +  | Addition |\n",
    "| -  | Subtraction |\n",
    "| /  | Division |\n",
    "| %  | Modulo | \n",
    "| *  | Multiplication | \n",
    "| //  | Floor division | \n",
    "| **  | To the power of |"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "5 % 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "5 // 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "5 ** 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Relational operators\n",
    "\n",
    "Besides arithmetic operators, there are also relational operators which compare values. They either return `True` or `False` according to the condition.\n",
    "\n",
    "| Symbol | Task Performed |\n",
    "|----|---|\n",
    "| == | True, if it is equal |\n",
    "| !=  | True, if not equal to |\n",
    "| < | less than |\n",
    "| > | greater than |\n",
    "| <=  | less than or equal to |\n",
    "| >=  | greater than or equal to |"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "z = 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "z == 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "z > 1"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Containers\n",
    "\n",
    "Containers are objects that hold an arbitrary number of other objects. Let's get to know some examples."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### [Lists](https://docs.python.org/3/tutorial/introduction.html#lists)\n",
    "A list stores a sequence of elements (Python objects) in a single object. It can be written as a list of comma-separated values (items/elements) between square brackets"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "colors = ['red', 'blue', 'green', 'black', 'white']\n",
    "type(colors)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Items of this list can be accessed by their position/index. Python uses **zero-based indexing** as in C (not 1 as in Fortran or Matlab). That means, the first element has an index 0, the second has index 1, and so on."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "colors[0]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "colors[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To enumerate elements from the tail of a list, we can use **negative indexing**. So -1 corresponds to the last element of the list, -2 to the second to last, and so on. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "colors[-1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we want to access multiple elements of a list (a sublist), we can use **slices** with the syntax `start:stop:step`.\n",
    "When you don't specify a `step`, the default value is 1.\n",
    "You will realise that slicing stops just **before** the specified `stop` index, so this index is excluded from the sublist."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "colors[2:4]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we skip the `start` number, it starts from index 0. Similarly, you can omit `stop` to go on until the end of the list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "colors[:3]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "['red', 'blue', 'green']"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "colors[0:3] # equivalent"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "colors[3:]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lists are **mutable** objects! So let's change one element of the original list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "colors[1] = \"rot\"\n",
    "colors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can do the same thing with multiple elements."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "colors[2:4] = ['gray', 'purple']\n",
    "colors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The list data type has some built-in [methods](https://docs.python.org/3/tutorial/datastructures.html#more-on-lists). For example, we can count the number of occurrences of an element in a list with `count()`. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(colors.count(\"red\"))\n",
    "colors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lists might contain items of different types."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mixed = [3.0, -200, 'hello']\n",
    "mixed"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "mixed.reverse()\n",
    "mixed"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lists also support operations like concatenation (`+`) or repetition (`*`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A = [\"Hi\"] * 5\n",
    "A"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A + mixed"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In case you want to remove all items from the list, you can use `clear()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "A.clear()\n",
    "A"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's have a look at a small **example:** \n",
    "\n",
    "Suppose we need to store the temperatures in Oslo, London, Paris and Aachen.\n",
    "\n",
    "***List solution***:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "temps = [13, 15.4, 17.5, -20]\n",
    "# temps[0]: Oslo\n",
    "# temps[1]: London\n",
    "# temps[2]: Paris\n",
    "# temps[3]: Aachen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### [Dictionaries](https://docs.python.org/3/tutorial/datastructures.html#dictionaries)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dictionaries can index objects in a collection via text, they are \"lists with text index\". The text indices are called **keys** and are immutable. So dictionaries can be thought of as a set of **key:value pairs**. Let's create our first dictionary to store some temperatures again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Initialize dictionary\n",
    "temps = {'Oslo': 13, 'London': 15.4, 'Paris': 17.5}\n",
    "\n",
    "# Applications\n",
    "print('The temperature in London is', temps['London'])\n",
    "print('The temperature in Oslo is',   temps['Oslo'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let's add the temperature in Aachen to the dictionary."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "temps['Aachen'] = -20.0\n",
    "temps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can iterate over the entries in a dictionary:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for city in temps:\n",
    "    print(\"The temperature in %s is %.2f\" % (city, temps[city]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To delete an entry (key and value), use the `del` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "del temps['Oslo']   # remove Oslo key w/value\n",
    "temps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's check whether we know the temperatures in Berlin and Aachen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"Berlin\" in temps"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"Aachen\" in temps"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### [Tuples](https://docs.python.org/3/tutorial/datastructures.html#tuples-and-sequences)\n",
    "A tuple consists of a number of values separated by commas."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = (1,2,3) # tuples, in contrast to lists, have round brackets\n",
    "a[0]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "They behave like lists, the main difference is that they are **immutable** objects, i.e. they can't be changed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b = a[:2]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "b[1] = \"test\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "ename": "TypeError",
     "evalue": "'tuple' object does not support item assignment",
     "output_type": "error",
     "traceback": [
      "\u001b[0;31m---------------------------------------------------------------------------\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m                                 Traceback (most recent call last)",
      "\u001b[0;32m<ipython-input-38-429c47ddcb33>\u001b[0m in \u001b[0;36m<module>\u001b[0;34m\u001b[0m\n\u001b[0;32m----> 1\u001b[0;31m \u001b[0mb\u001b[0m\u001b[0;34m[\u001b[0m\u001b[0;36m1\u001b[0m\u001b[0;34m]\u001b[0m \u001b[0;34m=\u001b[0m \u001b[0;34m\"test\"\u001b[0m\u001b[0;34m\u001b[0m\u001b[0;34m\u001b[0m\u001b[0m\n\u001b[0m",
      "\u001b[0;31mTypeError\u001b[0m: 'tuple' object does not support item assignment"
     ]
    }
   ],
   "source": [
    "b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In contrast, if we convert a tuple to a list, the **list is mutable**."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In contrast, if we convert a tuple to a list, the **list is mutable**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "c = list(a)\n",
    "c[1] = \"test\"\n",
    "c"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### [Strings](https://docs.python.org/3/tutorial/introduction.html#strings)\n",
    "Besides numbers, Python can also manipulate strings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 'test'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = \"Inverse modeling\"\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using triple quotes, string literals can span multiple lines."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s = \"\"\"Inverse\n",
    "modeling\"\"\"\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The string data type has some additional [methods](https://docs.python.org/3/library/stdtypes.html#string-methods), for example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s.upper()\n",
    "print(s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Strings can be indexed, sliced, concatenated (`+`), repeated (`*`), ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s[:7]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "s[-1]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "(s[:7] + ' Problem ') * 3"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"Inv\" in s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `%` operator can be used for [string formatting](https://docs.python.org/3/library/stdtypes.html#old-string-formatting), one example where this is extremely useful are dynamic file names."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\"An integer: %i; a float: %.2f; another string: %s\" % (1, 0.1, \"test\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "i = 102\n",
    "filename = 'processing_of_dataset_%d.txt' % i\n",
    "print(filename)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## [Control flow](https://docs.python.org/3/tutorial/controlflow.html#)\n",
    "\n",
    "The order, in which statements are evaluated/executed, is determined with control flow tools."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### [if/elif/else](https://docs.python.org/3/tutorial/controlflow.html#if-statements)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 3\n",
    "\n",
    "if a == 1:\n",
    "    print(\"a is one.\")\n",
    "elif a == 2:\n",
    "    print(\"a is two.\")\n",
    "else:\n",
    "    print(\"a is large.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### [for loops](https://docs.python.org/3/tutorial/controlflow.html#for-statements)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "adjectives = [\"powerful\", \"readable\", \"free\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Wrong\n",
    "for i in range(len(adjectives)):\n",
    "    print('Python is', adjectives[i])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The for loop above is unnecessarily complicated. A much simpler method is a plain for-in loop like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Correct!\n",
    "for word in adjectives:\n",
    "    print('Python is', word)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# If you need a number, use enumerate\n",
    "for i, word in enumerate(adjectives):\n",
    "    print(\"Word %d is %s.\" % (i, word))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Iterate over anything"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vowels = 'aeiouy'\n",
    "\n",
    "for letter in 'powerful':\n",
    "    if letter in vowels:\n",
    "        print(letter)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### [List comprehensions](https://docs.python.org/3/tutorial/datastructures.html#list-comprehensions)\n",
    "To make new lists where each element is the result of some operations applied to each member of another sequence or iterable, we use list comprehensions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0, 1, 4, 9]"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Wrong\n",
    "zahlen = []\n",
    "for i in range(4):\n",
    "    zahlen.append(i ** 2)\n",
    "    \n",
    "zahlen"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[0, 1, 4, 9]"
      ]
     },
     "execution_count": 12,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Correct!\n",
    "zahlen = [i**2 for i in range(4)]\n",
    "zahlen"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### [While loops](https://docs.python.org/3/reference/compound_stmts.html#the-while-statement)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 0\n",
    "\n",
    "while a < 10:\n",
    "    print(a)\n",
    "    a += 2.77"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Click [here](http://pythontutor.com/visualize.html#code=a%20%3D%200%0A%0Awhile%20a%20%3C%2010%3A%0A%20%20%20%20print%28a%29%0A%20%20%20%20a%20%2B%3D%202.77&cumulative=false&curInstr=0&heapPrimitives=false&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) to better understand what's happening."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Reusing code: Functions (classes, modules)\n",
    "## DRY: Don't repeat yourself\n",
    "To avoid repetitions and reuse code, we can define [functions](https://docs.python.org/3/tutorial/controlflow.html#defining-functions)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def test():\n",
    "    print(\"in test function\")\n",
    "\n",
    "test()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Print vs. return"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def double_it(x=2):\n",
    "    print(x * 2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def double_it2(x):\n",
    "    return x * 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = double_it(8)\n",
    "b = double_it2(8)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Global vs. function namespace\n",
    "When you define variables inside a function definition, they are local to this function by default."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "a = 2.\n",
    "\n",
    "def double_it(x, verbose=False, c=10):\n",
    "    double = 2 * x\n",
    "    if verbose:\n",
    "        print(\"Input:\", x, \"Output:\", double)\n",
    "    return double\n",
    "\n",
    "b = double_it(a, c=True, verbose=True)\n",
    "b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Click [here](http://pythontutor.com/visualize.html#code=a%20%3D%202.%0A%0Adef%20double_it%28x,%20verbose%3DFalse%29%3A%0A%20%20%20%20double%20%3D%202%20*%20x%0A%20%20%20%20if%20verbose%3A%0A%20%20%20%20%20%20%20%20print%28%22Input%3A%22,%20x,%20%22Output%3A%22,%20double%29%0A%20%20%20%20return%20double%0A%0Ab%20%3D%20double_it%28a%29&cumulative=false&curInstr=8&heapPrimitives=false&mode=display&origin=opt-frontend.js&py=3&rawInputLstJSON=%5B%5D&textReferences=false) to better understand what's happening."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Keyword arguments are optional"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "double_it(2)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Document your code/functions properly"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def funcname(param1, param2):\n",
    "    \"\"\"Concise one-line sentence describing the function.\n",
    "\n",
    "    Extended summary which can contain multiple paragraph\n",
    "    \n",
    "    Parameters\n",
    "    ----------\n",
    "    param1 : type\n",
    "        Description of parameter `param1`.\n",
    "    param2 : type\n",
    "        Description of parameter `param2`.\n",
    "    \"\"\"\n",
    "    # function body\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Importing other [packages](https://docs.python.org/3/tutorial/modules.html#packages)\n",
    "\n",
    "Sometimes you want to import a collection of modules, a so-called package."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy\n",
    "numpy.linspace(1,3,5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you want to obtain more details about an object, you can use question marks."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "numpy.linspace??"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are different ways of importing, dependent on your purpose."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from numpy import linspace\n",
    "np.linspace == linspace == numpy.linspace"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Never `import *` !\n",
    "\n",
    "Otherwise, you import a lot of stuff into your namespace, this might shadow some other objects, either built-in or from previous imports. It can make differences you might not be aware of. Let's compare, for example, Python's `sum` vs. NumPy's `sum`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 26,
   "metadata": {},
   "outputs": [],
   "source": [
    "def summe(a, b):\n",
    "    return a + b"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 27,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "7"
      ]
     },
     "execution_count": 27,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "summe(2,5)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 28,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[1, 2, 3, 2, 3, 4, 5, 7]"
      ]
     },
     "execution_count": 28,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "summe([1,2,3],[2,3,4,5,7])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 29,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "9"
      ]
     },
     "execution_count": 29,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "a = range(5)\n",
    "b = -1\n",
    "sum(a, b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 30,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "10"
      ]
     },
     "execution_count": 30,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.sum(a, b)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
