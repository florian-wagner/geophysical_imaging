import numpy as np
import matplotlib.pyplot as plt

def least_squares(G, d):
    """ Calculate and return least-squares solution."""
    GtG = G.T @ G
    m_est = np.linalg.inv(GtG) @ G.T @ d
    print('m_est:', m_est)
    return m_est

def plot(d, x, m_est, G):
    fig, ax = plt.subplots()
    ax.plot(x, d, "x", color="tab:red", label=r"Observed data $\mathbf{d}_\mathrm{obs}$")
    d_pre = G @ m_est
    ax.plot(x, d_pre, '.-', color='tab:blue', label=r"Predicted data $\mathbf{d}_\mathrm{pre}$")
    ax.set_xlabel('x (auxiliary variable)')
    ax.set_ylabel('y(x) (data)')
    ax.grid()
    ax.legend()
    return ax

def RMSE(d, d_pre):
    """ Calculate and return root-mean-square error."""
    RMSE = np.sqrt(1 / d.size * np.sum((d - d_pre) ** 2))
    return RMSE