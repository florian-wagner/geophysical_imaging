#!/usr/bin/env python
# coding: utf-8

# # Exercise 2 (Nov. 4, 2021)

# In[1]:


# Importing numpy for calculations and matplotlib for plotting
import numpy as np
import matplotlib.pyplot as plt


# # 1. Fitting a parabolic model

# The general form of a parabola can be written as $y(x) = a + b \cdot x + c \cdot x^2$.
# 
# Given are 25 measurements of a parabolic process, which are generated in the cell below (**do not change this cell**). You could think of them as "synthetic measurements", i.e. no real measurements from the field, but simulation results for feasibility studies for example. (The benefit of synthetic measurements is that we can compare our estimated model parameters to the true ones to evaulate the goodness of our inversion. This is something that is not possible in the field.)

# In[2]:


np.random.seed(23) # Make sure we always get the same random numbers
x = np.linspace(-15, 5, 25)
d_orig = -1.2 + 9.3 * x + 1.2 * x**2
d = d_orig + np.random.normal(scale=5, size=25)


# **a)** Construct the forward operator (i.e., the $\mathbf{G}$ matrix).

# **b)** Estimate $\mathbf{m}_\mathrm{est}$ for the given data.

# **c)** Plot measurement data and predicted data (i.e., the model response) and calculate the RMS error bewteen both quantities.

# **d)** What happens if you try to solve the problem above only using the first five measurements?

# **e)** Now solve the problem only using the first and the last measurement.

# ---
# 
# ## Bonus question
# ### The effect of data noise.
# 
# Revisit the first exercise from last week (i.e., the linear fit). We can also create our own, synthetic measurement data and check our model predictions for different noise nevels. We do this by first creating noise-free data, and then adding randomly distributed noise drawn from Numpy's [random](https://numpy.org/doc/stable/reference/random/generated/numpy.random.normal.html?highlight=rand%20normal#numpy.random.normal) module. 
# 
# First, create noise-free, synthetic data for the process $d(x) = 2.1*x + 5$.
# 
# Then add noise of different levels (`scale` argument) to the noise-free data. Invert the noisified data and visualize the results for different noise levels. Does the RMS, i.e. your data fit, get worse?

# In[10]:


d_orig = 2.1 * x + 5

G = np.array((x, np.ones(len(x)))).T

