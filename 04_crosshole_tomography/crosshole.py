#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from scipy.sparse import load_npz

# Load previously computed matrices (G and Wm are saved in sparse format to save space)
G = np.array(load_npz("G.npz").todense())
Wm = np.array(load_npz("Wm.npz").todense())
d = np.loadtxt("data.dat")

# Define acquisition geometry. Don't change this, as it was used to generate G.
bh_spacing = 20.0
bh_length = 25.0
sensor_spacing = 2.5
refinement = 0.25
x = np.arange(0, bh_spacing + refinement, sensor_spacing * refinement)
y = -np.arange(0.0, bh_length + 3, sensor_spacing * refinement)
depth = -np.arange(sensor_spacing, bh_length + sensor_spacing, sensor_spacing)
sensors = np.zeros((len(depth) * 2, 2))  # two boreholes
sensors[len(depth):, 0] = bh_spacing  # x
sensors[:, 1] = np.hstack([depth] * 2)  # y


#  Rays represent every possible combination of sensors amd receivers
from itertools import product
numbers = np.arange(len(depth))
rays = list(product(numbers, numbers + len(numbers)))
rays = np.array(rays)