#!/usr/bin/env python
# coding: utf-8

# In[1]:


get_ipython().run_line_magic('matplotlib', 'inline')
import numpy as np
import matplotlib.pyplot as plt


# # Exercise 6: Non-linear gravity inversion - Gradient Descent

# Consider the exact same non-linear inverse problem and the associated gravimetric data of the previous exercise:

# In[2]:


# Load data
dobs = np.loadtxt("data.dat")

# Define measurement positions, rho, G, and delta_gz
x = np.linspace(-50,50,50)
density = -3000 # cavity = decrease in density
G = 6.6743e-11 # gravitational constant

def grav(R, z):
    """Formula from the exercise sheet as a function for later reuse."""
    gz = 4/3 * np.pi * R**3 * density * G * z / (x**2 + z**2)**(3/2)
    gz *= 1e5 # unit conversion
    return gz


# In the previous exercise, we followed a grid-search procedure, meaning that we tested different parameter combinations by "brute-force" and checked how well they describe our measurements. The best possible combination, i.e. the minimum of our objective function, is the final solution.  
# #### *Use the "grid-search" code from last week!*

# ### Visualizing the prediction error for all combinations of R and z

# While this worked well and was relatively easy to implement (two for loops looping over the two parameters), it can be computationally prohibitive for real-work problems with hundreds or thousands of parameters and with a more complex forward problem (i.e., 3D simulation code rather than analytical formula). Today, we want to try a different, smarter approach.

# # Gradient descent

# As opposed to the global grid-search optimization shown above, [gradient descent](https://en.wikipedia.org/wiki/Gradient_descent) is a local optimization scheme. This approach, or variants of it, are commonly used in non-linear geophysical inversion, but also in other areas such as the training of neural networks. The main advantage of this method, as opposed to a grid-search, is that it usually requires much fewer executions of the forward function/model, thereby significantly improving it's efficiency. That is because only a small subset of the model space is explored.
# 
# The iterative algorithm starts with an initial guess, the starting model. In each iteration step, the gradient of the objective function in each dimension (i.e., with respect to each model parameter) is calculated. Since analytical solutions to this are often unavailable, this is commonly achieved by finite-difference calculations, meaning that the model is slightly varied and we check how the resonse changes. This gradient indicates in which way the model parameters should change in order to decrease the objective function. The exact amount by which the parameters are modified in each step is determined by a step length parameter. This iterative process is repeated until the objective function does not notably change anymore or the number of maximum iterations it reached.
# 
# Note that this scheme can be quite dependent on the initial model: If there are several minima to the objective function, gradient descent will converge towards the one closest to the starting model.

# Start by defining a central finite-difference approximation of the objective function's gradient (see the section on [relation with derivatives here](https://en.wikipedia.org/wiki/Finite_difference)) with respect to both model parameters (return a value for each!). The perturbation parameter $h$ should not change throughout the inversion and can be set to 0.1. Note that our objective function is a multi-variate function, meaning that it has two parameters (radius and depth). The gradient you need is:
# 
# $$\nabla \Psi(r,z) = \left[\frac{\partial}{\partial r} \Psi, \frac{\partial}{\partial z} \Psi\right]$$

# Define a function that calculates the gradient (with central finite-differences) and returns it as a numpy array at a given pair of $r$ and $z$.

# Define a starting model with a radius of 25 m and a depth of 40 m. Calculate the gradient at this position. It should be approximately [0.06, -0.02].

# Now you can implement the gradient descent. Define a starting model $m_0$, step-length $\alpha$ and the desired precision threshold. Model updates are calculated by multiplying the negative step length with the gradient. The procedure is as follows:
# 
# 1. Current model equals starting model $m = m_0$
# 
# 2. Calculate model update based on negative gradient $\Delta m = -\alpha \nabla\Psi(m)$
# 
# 3. Update model $m = m + \Delta m$
# 
# 4. Repeat from step 2
# 
# Start with a step length of $\alpha = 20$ and a maximum number of 1000 iterations. Visualize the current model estimate of each iteration on top of the figures of the grid search (as a simple point with `plt.plot`. Does your algorithm walk into the right direction? Does it reach the minimum?

# Visualize the convergence by plotting your error (`np.linalg.norm(dobs - grav(r_k, z_k))` history against the iteration number $k$.

# Play with the step size $\alpha$. What is its effect?
