{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Exercise 6: Non-linear gravity inversion"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Due to karstification processes in the subsurface, large cavities can form, which may ultimately collapse posing substantial risks to human life and infrastructures (See [this example](https://www.independent.co.uk/news/science/sinkholes-what-are-they-how-do-they-form-and-why-are-we-seeing-so-many-9136235.html) from Guatemala in 2010). Fortunately, cavities represent large density contrasts, such that they can be detected using gravimetric measurements (measuring local anomalies in gravitational acceleration). The dataset contains 50 equally spaced readings (given in mGal) along a profile from -50 to 50 meters. The anomaly is expected within the center of the profile. The analytical solution for a spheric anomaly in the subsurface is given by:\n",
    "\n",
    "$$\n",
    "\\Delta\\mathrm{g}_{\\mathrm{z}}=\\mathrm{G} \\frac{4}{3} \\pi \\mathrm{R}^{3} \\Delta \\rho \\frac{\\mathrm{z}}{\\left(\\mathrm{x}^{2}+\\mathrm{z}^{2}\\right)^{3 / 2}}\n",
    "$$\n",
    "\n",
    "where $\\Delta\\mathrm{g}_{\\mathrm{z}}$ is the vertical component of the gravimetric anomaly in m/s$^2$, $G$ is the gravitational constant (6.6743 $\\times 10^{-11}$ m$^3$/(kg $\\times$ s$^2$)), $R$ is the radius of the sphere in meter, $\\Delta \\rho$ is the density contrast between the anomaly and the surrounding rock in kg/m$^3$, $z$ is the depth of the center of the sphere, and $x$ is the location along the profile.\n",
    "Both $x$ and $z$ are in meters.\n",
    "\n",
    "We have the following a priori information:\n",
    "\n",
    "- The surrounding rock is expected to have a density of 3000 kg per cubic meters.\n",
    "- A basaltic bedrock is located at 50 meters depth.\n",
    "- Maximum radii of anomalies within the same region were measured to be 25 meters.\n",
    "\n",
    "\n",
    "Estimate the radius and the depth of the cavity using a grid-search procedure. Visualize the misfit (prediction error) as a function of both parameters. Is there a global minimum?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Loading and visualizing the measurements\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First the measured data is loaded"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "dobs = np.loadtxt(\"data.dat\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Take a first look at your gravity data by plotting it."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now define your auxiliary variables, such as measurement locations and all the constants you need, including your expected density contrast. Put the equation above in a Python function. This is your forward operator (i.e., G is a function now, not a matrix anymore!). Which parameters should the forward operator depend on? **Take care that your forward operator and the measured data have the same units!**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Trying some parameters and visualizing the model response"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can test different model hypotheses and simulate their response. Start with a constant radius and try different anomaly depths.\n",
    "Visualize the different models and their response in two separate plots. Some plotting advice:\n",
    "\n",
    "You can create several subplots by using fig, ax = [plt.subplots()](https://matplotlib.org/3.3.3/api/_as_gen/matplotlib.pyplot.subplots.html). \n",
    "\n",
    "To get the same color for the same model in both plots you can store the first one like: line, = ax.plot(x, y). By using color=line.get_color() in the second plot's arguments you ensure your model has the same color as the corresponding resonse.\n",
    "\n",
    "*Plotting Hints*  \n",
    "* You can create circular objects using [plt.Circle()](https://matplotlib.org/3.3.3/api/_as_gen/matplotlib.patches.Circle.html?highlight=circle#matplotlib.patches.Circle) \n",
    "* and add them to a subplot via [ax.add_artist()](https://matplotlib.org/3.3.3/api/_as_gen/matplotlib.axes.Axes.add_artist.html?highlight=add_artist#matplotlib.axes.Axes.add_artist).  \n",
    "\n",
    "* In order to keep the aspect ratio equal you can use [ax.set_aspect()](https://matplotlib.org/stable/api/_as_gen/matplotlib.axes.Axes.set_aspect.html) "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Grid search (brute force) procedure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that the forward operator is working the full grid search can commence.\n",
    "\n",
    "Think of useful value ranges to test for each parameter, which you can then create using [np.linspace](https://numpy.org/doc/stable/reference/generated/numpy.linspace.html?highlight=linspace#numpy.linspace). Check the given a priori information above.\n",
    "\n",
    "Given a set of parameter ranges, the [meshgrid](https://numpy.org/doc/stable/reference/generated/numpy.meshgrid.html) function can create two grids in matrix form that reflect the possible parameter combinations. This is useful to determine the corresponding parameters once you have found an optimum. \n",
    "\n",
    "To find an optimum you also need to create an objective function that returns the norm of the prediction error, as well as an appropriately shaped array to store the objective function evaluations. The [argmin](https://numpy.org/doc/stable/reference/generated/numpy.argmin.html?highlight=argmin#numpy.argmin) function helps you find the index of the optimum.   \n",
    "*Hint: you can use [np.unravel_index()](https://numpy.org/doc/stable/reference/generated/numpy.unravel_index.html) to get the coordinates of the optimal parameters*\n",
    "\n",
    "To loop over a set of items while also getting their indices returned, use the [enumerate](https://www.programiz.com/python-programming/methods/built-in/enumerate) function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Visualizing the prediction error for all combinations of R and z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can now plot the grid search results. Besides the optimum you should also have a matrix with the norm of prediction errors, which you can plot [discretely](https://matplotlib.org/3.3.3/api/_as_gen/matplotlib.pyplot.imshow.html) or [continuously](https://matplotlib.org/3.3.3/api/_as_gen/matplotlib.axes.Axes.pcolor.html?highlight=pcolor#matplotlib.axes.Axes.pcolor). Plot the minimum into the parameter space and highlight the corresponding parameters by drawing [horizontal](https://matplotlib.org/3.3.3/api/_as_gen/matplotlib.pyplot.hlines.html?highlight=hline#matplotlib.pyplot.hlines) and [vertical](https://matplotlib.org/3.3.3/api/_as_gen/matplotlib.pyplot.vlines.html#matplotlib.pyplot.vlines) lines from the minimum toward the axes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Visualizing final result (global minimum)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now visualize the model corresponding to the optimum. Plot the response and the model into separate coorinate systems like you did above."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "--- \n",
    "## Bonus"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The grid search procedure above is also available in the `scipy.optimize` package, and has direct parallelization of the different function calls: https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.brute.html\n",
    "Actually all approaches discussed in the lecture on non-linear optimization are in the scipy optimize package. The [`minimize`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.minimize.html) function provides a unified interface to them, which we can use here to solve our problem more efficiently. Try it out!"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Tags",
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.10"
  },
  "toc-autonumbering": false,
  "toc-showcode": true,
  "toc-showmarkdowntxt": true,
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "window_display": false
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
